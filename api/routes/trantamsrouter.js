import { Router } from 'express';
import { body } from 'express-validator';
import tranController from '../controllers/trantamscontroller';

const router = Router();

router.post('/history', 
    [
        body('username')
            .not().isEmpty().isEmail().withMessage('username is required || an email')
            .trim()
            .escape(),
        body('wallet')
            .not().isEmpty().isString().withMessage('wallet is required || string format')
            .trim()
            .escape(),
        body('password')
            .not().isEmpty().isString().withMessage('password is required || string format')
            .trim()
            .escape(),
        body('viewWallet')
            .optional()
            .isString().withMessage('string format'),
        body('startDate')
            .optional()
            .toDate(),
        body('endDate')
            .optional()
            .toDate(),
        body('limit')
            .optional()
            .isInt({ min: 1, max:1000 }).withMessage('int is required or min: 1, max: 1000 allowed'),
        body('currentPage')
            .optional()
            .isInt({ min:1 }).withMessage('int is required or min: 1')
    ]
    ,tranController.getTransactionTams
);

export default router;