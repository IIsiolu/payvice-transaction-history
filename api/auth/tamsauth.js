import crypto from 'crypto';
import axios from 'axios';
import parser from 'xml2json';

const  tranAuth = {

    async tamsAuth(wallet, username, password){
        
        
        /*
        * 
        * authenticating with unencrypted password
        * 
        */
        try{
            const terminal_id = wallet;
            const user_id = username;
            const passwd = password;    
            const url = process.env.TAMS_ADVICE;
            const paramsINIT = '?action=TAMS_LOGIN&termid='+terminal_id+'&userid='+user_id+'&control=INIT';
            const url1 = url.concat(paramsINIT);
            const response = await axios(url1);
            if(response.status != 200){
                return {
                    error: true,
                    message: 'An error has occured, please try again'
                }
            }

            let options = {
                object: true,
                sanitize: true,
                trim: true,
            };
            const json = parser.toJson(response.data, options);
            
            if(!json.efttran || !json.efttran.tran){
                return {
                    error: true,
                    message: 'An error occured, please try again'
                }
            }

            if(json.efttran.tran.result != 0){
                return {
                    error: true,
                    message: json.efttran.tran.message
                }
            }
            // create encrypted password
            const userTID = json.efttran.tran.macros_tid;
            const message = json.efttran.tran.message;
            const key = message.split('|')[1];
            let clrkey = '';

            userTID.split('').forEach((tid)=>{
                if(isNaN(tid)){
                    tid = 7;
                }
                clrkey += key.substr(tid, 1);
                // console.log(clrkey);
            });
            
            let hexbin = hex_to_ascii(clrkey);
            hexbin += passwd;
        
            const passwordHash = crypto.createHash('sha256').update(hexbin, "ascii").digest('hex');
            const paramsAUTH = '?action=TAMS_LOGIN&termid='+terminal_id+'&userid='+user_id+'&password='+passwordHash;
            const url2 = url.concat(paramsAUTH);
        
            const response1 = await axios(url2);
            if(response1.status != 200){
                return {
                    error: true,
                    message: 'An error occured, please try again'
                }
            }
    
            let option = {
                object: true,
                sanitize: true,
                trim: true,
            };
    
            const json1 = parser.toJson(response1.data, option);
            if(!json1.efttran || !json.efttran.tran){
                return {
                    error: true,
                    message: 'An error occured, please try again'
                }
            }
    
            if(json1.efttran.tran.result != 0){
                return {
                    error: true,
                    message: json.efttran.tran.message
                }
            }      
            return true;
        }catch(errors){
            return {
                error: true,
                message: errors
            };
        }
    }
};

function hex_to_ascii(str1){
    var hex = str1.toString();
    var str = '';
    for(var n =0; n < hex.length; n+=2){
        str += String.fromCharCode(parseInt(hex.substr(n, 2), 16));
    }
    return str;
}

export default tranAuth;