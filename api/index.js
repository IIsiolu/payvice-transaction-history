import express from 'express';
import tranRoutes from './routes/trantamsrouter';
const port = process.env.PORT || 5000;

const app = express();
app.use(express.json());

app.get('/', (req, res)=>{
    res.send('Welcome to tams transaction history request api');
});

app.use('/api/v1/transacttams', tranRoutes);

app.listen(port, ()=>{
    console.log(`App listening on port ${port}...`);
});

export default app;