import tranAuth from '../auth/tamsauth';
import axios from 'axios';

const tranService = {
    async getTransactionTams(tranDetails){
        try{
            // tams authentication
            let today = new Date();
            let date = today.getFullYear()+'-'+("0" + (today.getMonth() + 1)).slice(-2)+'-'+("0" + today.getDate()).slice(-2);

            const { wallet, username, password, viewwallet, startdate, enddate, limit, currentpage, product, productname } = tranDetails;
            const walletID = wallet ? wallet : wallet;
            const viewWallet = viewwallet ? viewwallet : wallet;
            const startDate = startdate ? startdate : date;
            const endDate = enddate ? enddate : date;
            const limitValue = limit ? limit : 100;
            const currentPage = currentpage ? currentpage : 1;
            const productValue = product ? product : 'ALL';
            const productName = productname ? productname : 'ALL';

            const parameters = {
                "walletID": walletID,
                "viewWallet": viewWallet,
                "startDate": startDate,
                "endDate": endDate,
                "limit": limitValue,
                "currentPage": currentPage,
                "product": productValue,
                "productName": productName
            }
       
            const authUser = await tranAuth.tamsAuth(walletID, username, password);
            console.log("user login successful ", authUser);
            if(authUser != true){
                return authUser;
            }
        
            const url = process.env.TAMS_URL+'?walletID='+walletID;
            console.log("tams trans url ", url);
            console.log(parameters);

            const response = await axios(url, { method: 'post', data: parameters });
            console.log("status code: ", response);

            if(response.status != 200){
                return {
                    error: true,
                    message: 'An error occured, please try again'
                }
            }
            
            if(!response.data){
                return {
                    error: true,
                    message: 'An error occured, please try again'
                }
            }

            // if(json.efttran.tran.result != 0){
            //     return {
            //         error: true,
            //         message: json.efttran.tran.message
            //     }
            // }
            console.log("response data: ",response.data);
            return response.data;
        } catch (errors) {
            return {
                error: true,
                message: errors
            }
        }
    }
};

export default tranService;