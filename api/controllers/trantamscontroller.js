import tranService from '../services/trantamsservice';
import { validationResult } from 'express-validator';

const tranController = {
    async getTransactionTams(req, res){
        try{
            const errors = validationResult(req);
            if(!errors.isEmpty()){
                return res.status(422).json({ errors: errors.array() });
            }

            const tranResult = await tranService.getTransactionTams(req.body);
            
            if(tranResult.error == true){
                return res
                    .json({
                        status: 401,
                        error: tranResult.message
                    })
                    .status(401);
            }
            return res
                .json({
                    status: 201,
                    data: tranResult
                })
                .status(201);
        }catch(error){
            return res.status(401).json(error);
        }
    }
};

export default tranController;